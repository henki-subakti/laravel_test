<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'user_id', 'title', 'content', 'is_draft', 'is_members_only', 'posted_at'
    ];

    public function author() {
        return $this->hasOne(User::class,'id','user_id');
    }
}
