<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Post;
use Carbon\Carbon;
use Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the users' posts.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if( !Auth::check() ) { 
            return redirect()->route('posts.index');
        }

        $query_post = Post::with('author')
                    ->where('user_id',Auth::user()->id);

        $posts      = $query_post->get();

        $data   = [
            "posts"     => $posts
        ];

        return view('home', $data);
    }
}
