<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use Carbon\Carbon;
use Auth;

class PostController extends Controller
{

    public function __construct() {
        $this->middleware('auth', ['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $query_post = Post::with('author')
                    ->where('is_draft',0)
                    ->whereDate('posted_at','<=',Carbon::today())
                    ->orderBy('created_at','DESC');
                    
        if( !Auth::check() ) { // user not login
            $query_post->where('is_members_only','!=',1);
        }

        $posts      = $query_post->paginate(10);

        $data   = [
            "posts"     => $posts
        ];

        return view('posts.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data_post  = [
            "user_id"           => Auth::user()->id,
            "title"             => $request->title,
            "content"           => $request->content,
            "is_draft"          => ($request->is_draft != null) ? 1 : 0,
            "is_members_only"   => ($request->is_members_only != null) ? 1 : 0,
            "posted_at"         => Carbon::createFromFormat('Y-m-d', $request->posted)->format('Y-m-d H:i:s'),
        ];
        
        Post::create($data_post);

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $query_post = Post::with('author')
                    ->where('id',$id)
                    ->where('is_draft',0)
                    ->whereDate('posted_at','<=',Carbon::today());
                    
        if( !Auth::check() ) { // user not login
            $query_post->where('is_members_only','!=',1);
        }

        $post      = $query_post->first();

        if( !isset($post->id) ) {
            return redirect('/');
        }

        $data 	= [
			'post_title'	=> $post->title,
            'post_content'	=> $post->content,
            'author'	    => $post->author->name,
            'post_date'	    => Carbon::parse($post->created_at)->format('Y-m-d'),
		];

        return view('posts.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user_id        = Auth::user()->id;
        $post           = Post::with(['author'])->find($id);

        if( !isset($post->id) ) {
            return redirect('/');
        }

        if( $post->user_id != $user_id ) {
            return redirect('/');
        }

        $data 	= [
            'id'                => $id,
			'post_title'	    => $post->title,
            'post_content'	    => $post->content,
            'author'	        => $post->author->name,
            'is_draft'          => $post->is_draft,
            'is_members_only'   => $post->is_members_only,
            'post_date'	        => Carbon::parse($post->created_at)->format('Y-m-d'),
		];

        return view('posts.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $user_id    = Auth::user()->id;
        $find       = Post::find($id);

        if( !isset($find->id) ) {
            return redirect('/');
        }

        if( $find->user_id != $user_id ) {
            return redirect('/');
        }

        $data_post  = [
            "user_id"           => Auth::user()->id,
            "title"             => $request->title,
            "content"           => $request->content,
            "is_draft"          => ($request->is_draft != null) ? 1 : 0,
            "is_members_only"   => ($request->is_members_only != null) ? 1 : 0,
            "posted_at"         => Carbon::createFromFormat('Y-m-d', $request->posted)->format('Y-m-d H:i:s'),
        ];
        
        $find->update($data_post);

        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $user_id    = Auth::user()->id;
        $find       = Post::find($id);
        
        if( !isset($find->id) ) {
            return redirect('/');
        }

        if( $find->user_id != $user_id ) {
            return redirect('/');
        }

        $find->delete();
        
        return redirect()->route('home');
    }
}
