@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3>Post Detail</h3>
            <div class="card mt-3">
                <div class="card-header">{{ $post_title }}</div>
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <span>{{ $author }}</span>
                        <span>{{ $post_date }}</span>
                    </div>
                    <hr>
                    <div>
                        {!! $post_content !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
