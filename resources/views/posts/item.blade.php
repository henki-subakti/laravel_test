<div class="card mt-3">
    <div class="card-header"><a href="{{ route('posts.show',[$val->id]) }}">{{ $val->title }}</a></div>
    <div class="card-body d-flex justify-content-between">
        <span>{{ $val->author->name }}</span>
        <span>{{ $val->created_at }}</span>
    </div>
</div>
