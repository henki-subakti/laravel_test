@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            {{-- If logged-in user has posts, show all posts written by the user. --}}
            <h3>Your Posts</h3>
            @foreach( $posts as $key => $val )
            <div class="card mt-3">
                <div class="card-header"><a href="{{ route('posts.show',[$val->id]) }}">{{ $val->title }}</a></div>
                <div class="card-body">
                    <span class="badge badge-{{ ($val->is_draft == 1) ? "secondary" : "primary"  }}">{{ ($val->is_draft == 1) ? "Draft" : "Published" }}</span>
                    @if( $val->is_members_only )
                    <span class="badge badge-success">Members Only</span>
                    @endif
                    <span class="ml-2">Posted: {{ \Carbon\Carbon::parse($val->posted_at)->format('Y-m-d') }}</span> / <span>Updated: {{ \Carbon\Carbon::parse($val->updated_at)->format('Y-m-d') }}</span>
                    <div class="mt-3">
                        <a href="{{ route('posts.show',[$val->id]) }}">Detail</a> /
                        <a href="{{ route('posts.edit',[$val->id]) }}">Edit</a> /
                        <form method="POST" class="d-inline" action="{{ route('posts.destroy',[$val->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-link p-0 border-0 text-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>

            {{-- <div class="card mt-3">
                <div class="card-header"><a href="{{ route('posts.show',[$val->id]) }}">{{ $val->title }}</a></div>
                <div class="card-body">
                    <span class="badge badge-primary">Published</span>
                    <span class="ml-2">Posted: {{ \Carbon\Carbon::parse($val->posted_at)->format('Y-m-d') }}</span> / <span>Updated: {{ \Carbon\Carbon::parse($val->updated_at)->format('Y-m-d') }}</span>
                    <div class="mt-3">
                        <a href="{{ route('posts.show',[$val->id]) }}">Detail</a> /
                        <a href="{{ route('posts.edit',[$val->id]) }}">Edit</a> /
                        <form method="POST" class="d-inline" action="{{ route('posts.destroy',[$val->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-link p-0 border-0 text-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-header"><a href="{{ route('posts.show',[$val->id]) }}">{{ $val->title }}</a></div>
                <div class="card-body">
                    <span class="badge badge-secondary">Draft</span>
                    <span class="ml-2">Posted: {{ \Carbon\Carbon::parse($val->posted_at)->format('Y-m-d') }}</span> / <span>Updated: {{ \Carbon\Carbon::parse($val->updated_at)->format('Y-m-d') }}</span>
                    <div class="mt-3">
                        <a href="{{ route('posts.show',[$val->id]) }}">Detail</a> /
                        <a href="{{ route('posts.edit',[$val->id]) }}">Edit</a> /
                        <form method="POST" class="d-inline" action="{{ route('posts.destroy',[$val->id]) }}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-link p-0 border-0 text-danger">Delete</button>
                        </form>
                    </div>
                </div>
            </div> --}}
            @endforeach

            {{-- If logged-in user doesn't have posts, show sentence below. --}}
            @auth
            <p> @if( count($posts) == 0 )You have no posts yet. @endif Create a new post <a href="{{ route('posts.create') }}">here</a>.</p>
            @endauth

            {{-- If guest users, show sentence below. --}}
            @guest
            <p>Please login <a href="{{ route('login') }}">here</a>.</p>
            <p>If you don't have an account, register <a href="{{ route('register') }}">here</a>.</p>
            @endguest

        </div>
    </div>
</div>
@endsection
